let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/

   mix.styles([
      'resources/assets/plantilla/css/font-awesome.min.css',
      'resources/assets/plantilla/css/simple-line-icons.min.css',
      'resources/assets/plantilla/css/style.css',
   ], 'public/css/plantilla.css')
   .scripts([
      'resources/assets/plantilla/js/jquery.min.js',
      'resources/assets/plantilla/js/popper.min.js',
      'resources/assets/plantilla/js/bootstrap.min.js',
      'resources/assets/plantilla/js/Chart.min.js',
      'resources/assets/plantilla/js/pace.min.js',
      'resources/assets/plantilla/js/template.js',
      'resources/assets/plantilla/js/sweetalert2.js',
   ], 'public/js/plantilla.js')
   .js('resources/assets/js/app.js', 'public/js/app.js');

   mix.styles([
      'resources/assets/datatable/css/dataTables.bootstrap4.min.css',
      'resources/assets/datatable/css/buttons.dataTables.min.css',
      'resources/assets/datatable/css/responsive.bootstrap.min.css',
   ], 'public/css/datatable.css')
   .scripts([
      'resources/assets/datatable/js/jquery.dataTables.min.js',
      'resources/assets/datatable/js/dataTables.bootstrap4.min.js',
      'resources/assets/datatable/js/dataTables.buttons.min.js',
      'resources/assets/datatable/js/dataTables.responsive.min.js',
      'resources/assets/datatable/js/buttons.html5.min.js',
      'resources/assets/datatable/js/jszip.min.js',
      'resources/assets/datatable/js/pdfmake.min.js',
      'resources/assets/datatable/js/vfs_fonts.js',
   ], 'public/js/datatable.js');